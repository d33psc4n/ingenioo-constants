export const paymentActions = {
  createSubscription: 'createSubscription',
  attachPaymentMethod: 'attachPaymentMethod',
  changeDefaultPaymentMethod: 'changeDefaultPaymentMethod',
  createConnectAccount: 'createConnectAccount',
  resetConnectAccount: 'resetConnectAccount',
  getExpressDashboardLink: 'getExpressDashboardLink'
}

export const stripeExpressTokenFields = {
  token: 'token',
  accountId: 'accountId',
}
