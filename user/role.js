const roleFields = {
  name: 'name',
  users: 'users',
  isPrimary: 'isPrimary',
  displayName: 'displayName',
  roles: 'roles',
  isCourse: 'isCourse',
}

const roleActions = {}

export { roleFields, roleActions }
