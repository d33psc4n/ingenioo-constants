export enum userFields {
  firstName = 'firstname',
  unreadNotifications = 'unreadNotifications',
  unreadNotificationsCount = 'unreadNotificationsCount',
  lastName = 'lastname',
  avatar = 'avatar',
  phoneNumber = 'phoneNumber',
  notificationClasses = 'notificationClasses',
  notificationEmail = 'notificationEmail',
  notificationInstant = 'notificationInstant',
  language = 'language',
  username = 'username',
  password = 'password',
  birthDate = 'birthDate',
  email = 'email',
  gender = 'gender',
  children = 'children',
  parent = 'parent',
  active = 'active',
  emailVerified = 'emailVerified',
  phoneVerified = 'phoneVerified',
  type = 'type',
  paymentData = 'paymentData',
}

const paymentDataFields = {
  connectAccountId: 'connectAccountId',
  connectAccountEnabled: 'connectAccountEnabled',
  connectAccountDetailsSubmitted: 'connectAccountDetailsSubmitted',
  stripeCustomerId: 'stripeCustomerId',
  user: 'user',
}

// const userActions = {
//   editUser: 'editUser',
//   changeAvatar: 'changeAvatar',
//   deleteUser: 'deleteUser',
//   createUser: 'createUser',
//   setNotificationSettings: 'setNotificationSettings',
//   setUserGroups: 'setUserGroups',
//   getUsersInGroup: 'getUsersInGroup',
//   getUsersNotInGroup: 'getUsersNotInGroup',
//   getUsersForGroups: 'getUsersForGroups',
//   addRemoveUsersToGroup: 'addRemoveUsersToGroup',
//   getAllUserGroups: 'getAllUserGroups',
//   updateAllUserNotifications: 'updateAllUserNotifications',
//   editProfile: 'editProfile',
//   getAllUsers: 'getAllUsers',
//   changePassword: 'changePassword',
//   changePasswordAdmin: 'changePasswordAdmin'
// };

const userActions = {
  signUp: 'signUp',
  associateChild: 'associateChild',
  verifyPhoneNumber: 'verifyPhoneNumber',
  login: 'login',
  editProfile: 'editProfile',
  changeAvatar: 'changeAvatar',
  changePassword: 'changePassword',
  usernameAvailable: 'usernameAvailable',
  getSearchToken: 'getSearchToken',
  updateTosPPTimestamp: 'updateTosPPTimestamp',
}

export { userActions, paymentDataFields }
