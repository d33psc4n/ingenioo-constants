import { fileUploadActions } from './app/fileUpload.js'
import { schoolActions } from './app/school.js'
import { userActions } from './user/user.js'
import { roleActions } from './user/role.js'
import { chatActions } from './chat/index.js'
import { meetingActions } from './meeting/index.js'
import { notificationActions, notificationClasses } from './app/notification.js'
import { feedbackActions } from './app/feedback.js'
import { attendanceActions } from './class/attendance.js'
import { courseFeedbackActions } from './class/courseFeedback.js'
import { gradeActions } from './class/grade.js'
import { quizItemTypes, quizTeacherActions } from './quiz/teacher.js'

import { quizStudentActions } from './quiz/student.js'

import { assignmentActions } from './class/assignment.js'
import { courseActions } from './class/course.js'
import { announcementActions } from './class/announcement.js'
import { postActions } from './class/post.js'
import { classRoomActions } from './class/classRoom.js'
import { timetableActions } from './class/timetable.js'
import { paymentActions } from './payment/index.js'
import * as fields from './fields.js'

const actions = {
  ...announcementActions,
  ...assignmentActions,
  ...attendanceActions,
  ...chatActions,
  ...classRoomActions,
  ...courseActions,
  ...courseFeedbackActions,
  ...feedbackActions,
  ...fileUploadActions,
  ...gradeActions,
  ...meetingActions,
  ...notificationActions,
  ...paymentActions,
  ...postActions,
  ...quizStudentActions,
  ...quizTeacherActions,
  ...roleActions,
  ...schoolActions,
  ...timetableActions,
  ...userActions,
}

enum tableNames {
  announcement = 'Announcement',
  assignment = 'Assignment',
  assignmentFeedback = 'AssignmentFeedback',
  assignmentSubmission = 'AssignmentSubmission',
  attendance = 'Attendance',
  averageOverride = 'AverageOverride',
  chat = 'Chat',
  childInvite = 'ChildInvite',
  classRoom = 'ClassRoom',
  classWork = 'ClassWork',
  course = 'Course',
  courseFeedback = 'CourseFeedback',
  fcmToken = 'FCMToken',
  feedback = 'Feedback',
  fileMetadata = 'FileMetadata',
  fileUpload = 'FileUpload',
  grade = 'Grade',
  gradeType = 'GradeType',
  meetingServer = 'MeetingServer',
  message = 'Message',
  notification = 'Notification',
  notificationClass = 'NotificationClass',
  notificationRoute = 'NotificationRoute',
  paymentData = 'PaymentData',
  post = 'Post',
  quiz = 'Quiz',
  quizBlankAnswer = 'QuizBlankAnswer',
  quizBlankItem = 'QuizBlankItem',
  quizChoiceOption = 'QuizChoiceOption',
  quizDropdownAnswer = 'QuizDropdownAnswer',
  quizDropdownItem = 'QuizDropdownItem',
  quizItem = 'QuizItem',
  quizItemType = 'QuizItemType',
  quizQuestion = 'QuizQuestion',
  quizStatus = 'QuizStatus',
  quizTake = 'QuizTake',
  quizTextAnswer = 'QuizTextAnswer',
  quizUploadAnswer = 'QuizUploadAnswer',
  role = '_Role',
  room = 'Room',
  schoolData = 'SchoolData',
  stripeExpressToken = 'StripeExpressToken',
  smsVerify = 'SmsVerify',
  subject = 'Subject',
  timetable = 'Timetable',
  timetableItem = 'TimetableItem',
  unreadChat = 'UnreadChat',
  user = '_User',
  userType = 'UserType',
  zoomRoom = 'ZoomRoom',
}

const primaryRoleNames = {
  administrator: 'Administrator',
  classMaster: 'ClassMaster',
  parent: 'Parent',
  secretary: 'Secretary',
  student: 'Child',
  teacher: 'Teacher',
}

enum userTypes {
  PARENT = 'parent',
  STUDENT = 'student',
  TEACHER = 'teacher',
  ADMIN = 'admin',
}

const masterObj = { useMasterKey: true }

const errors = {
  notAllowed: 'Not allowed.',
}

const passwordChangeTypes = {
  elev: 'elev',
  mama: 'mama',
  tata: 'tata',
}

// defined minimum role required to perform a CF
const actionPermissions = {
  HOOK: -1,
  STUDENT: 0,
  PARENT: 1,
  TEACHER: 2,
  CLASSMASTER: 3,
  SECRETARY: 4,
  ADMIN: 5,
}

export * from './fields.js'

export {
  actionPermissions,
  actions,
  errors,
  fields,
  masterObj,
  notificationClasses,
  passwordChangeTypes,
  primaryRoleNames,
  quizItemTypes,
  tableNames,
  userTypes,
}
