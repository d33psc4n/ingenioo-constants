import Parse from 'parse'

export const ERROR_OVERRIDE_CODE = -2

export class IngError extends Error {
  code: number

  consoleMessage: string | undefined

  constructor(code: number, consoleMessage?: string) {
    super('')
    this.code = code
    this.consoleMessage = consoleMessage
  }
}

export enum CustomError {
  /** User is banned */
  BANNED = 1000,
  /** Default error */
  GENERAL_ERROR,
  /** User is not allowed */
  NOT_ALLOWED,
  /** User is not auth */
  NOT_AUTHENTICATED,
  /** The permission set in the clour request doesn't exist */
  PERMISSION_DOES_NOT_EXIST,
  /** Requests too fast, thrown from debounce */
  TOO_FAST,
  /** The connect account is not enabled */
  CONNECT_ACCOUNT_NOT_ENABLED,
  /** Cannot delete the connect account (from stripe if there's a balance waiting i.e.) */
  CONNECT_ACCOUNT_CANNOT_DELETE,
  /** Child is not 14 */
  USER_UNDER_AGE,
  /** The invite token for the parent has expired */
  INVITE_TOKEN_EXPIRED,
  /** The SMS code has expired */
  SMS_VERIFICATION_CODE_EXPIRED,
  /** The SMS code is invalid */
  INVALID_SMS_VERIFICATION_CODE,
  /** The old password required to change is invalid */
  INVALID_OLD_PASSWORD,
  /** Recaptcha error */
  RECAPTCHA_ERROR,
  /** There was an error with logging in */
  LOGIN_ERROR,
  /** Cannot remove default payment method because subscription is active */
  REMOVE_DEFAULT_PAYMENT_METHOD_SUBSCRIPTION_ACTIVE,
  /** General joi error */
  INPUT_VALIDATION_ERROR,
}

const _upperToCamel = (name: string) =>
  name
    .toLowerCase()
    .split('_')
    .map((word, i) => (i === 0 ? word : word.charAt(0).toUpperCase() + word.slice(1)))
    .join('')

const errors: Map<number, string> = new Map()

const _setErrorMapForObj = (obj: Record<string, any>) => {
  Object.entries(obj).forEach(([k, v]) => {
    errors.set(v, _upperToCamel(k))
  })
}

_setErrorMapForObj(Parse.Error)
_setErrorMapForObj(CustomError)

export const getErrorForCode = (code: number, ...args: (string | number)[]): string => {
  const baseError = `errors.${errors.get(code) ?? 'otherCause'}`
  return args.length ? `${baseError}|${args.join(',')}` : baseError
}
