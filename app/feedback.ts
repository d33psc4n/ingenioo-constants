const feedbackFields = {
  author: 'author',
  description: 'description',
  type: 'type',
  severity: 'severity',
  steps: 'steps',
  userAgent: 'userAgent',
  module: 'module',
  files: 'files',
}

enum FEEDBACK_TYPES {
  bug = 'bug',
  suggestion = 'suggestion',
}

const feedbackActions = {
  submitFeedback: 'submitFeedback',
}

export { feedbackFields, feedbackActions, FEEDBACK_TYPES }
