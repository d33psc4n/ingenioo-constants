const fileUploadFields = {
  author: 'author',
  name: 'name',
  file: 'file',
  metadata: 'metadata',
}

const fileMetadataFields = {
  fileName: 'fileName',
  metadata: 'metadata',
}

const fileUploadActions = { deleteFileUpload: 'deleteFileUpload' }

export { fileUploadFields, fileUploadActions, fileMetadataFields }
