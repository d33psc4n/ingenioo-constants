const notificationFields = {
  target: 'target',
  title: 'title',
  content: 'content',
  author: 'author',
  route: 'route',
  actionObject: 'actionObject',
}

const fcmTokenFields = {
  token: 'token',
  user: 'user',
  identifier: 'identifier',
  deviceInfo: 'deviceInfo',
}

const notificationClassFields = {
  name: 'name',
  displayName: 'displayName',
}

const notificationClasses = {
  chat: 'Chat',
  grade: 'Grade',
  room: 'Room',
  assignment: 'Assignment',
  attendance: 'Attendance',
  post: 'Post',
}

const notificationRouteFields = {
  name: 'name',
}

const notificationActions = {
  deleteUnreadNotifications: 'deleteUnreadNotifications',
  addFCMToken: 'addFCMToken',
  deleteFCMToken: 'deleteFCMToken',
}

export {
  notificationClasses,
  fcmTokenFields,
  notificationClassFields,
  notificationFields,
  notificationRouteFields,
  notificationActions,
}
