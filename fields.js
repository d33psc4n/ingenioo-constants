import { fileUploadFields, fileMetadataFields } from './app/fileUpload.js'
import { schoolFields } from './app/school.js'
import { userFields, paymentDataFields } from './user/user.js'
import { roleFields } from './user/role.js'
import { chatFields, unreadChatFields, messageFields } from './chat/index.js'
import { roomFields, zoomRoomFields, meetingServerFields } from './meeting/index.js'
import {
  notificationClassFields,
  notificationFields,
  notificationRouteFields,
  fcmTokenFields,
} from './app/notification.js'
import { feedbackFields } from './app/feedback.js'
import { attendanceFields } from './class/attendance.js'
import { courseFeedbackFields } from './class/courseFeedback.js'
import { gradeFields, gradeTypeFields, averageOverrideFields } from './class/grade.js'
import {
  quizBlankItemFields,
  quizChoiceOptionFields,
  quizDropdownItemFields,
  quizFields,
  quizItemFields,
  quizItemTypeFields,
  quizQuestionFields,
  quizStatusFields,
  quizStatuses,
} from './quiz/teacher.js'

import {
  quizBlankAnswerFields,
  quizDropdownAnswerFields,
  quizTakeFields,
  quizTextAnswerFields,
  quizUploadAnswerFields,
} from './quiz/student.js'

import {
  assignmentFeedbackFields,
  assignmentFields,
  assignmentSubmissionFields,
} from './class/assignment.js'
import { subjectFields, courseFields, classWorkFields } from './class/course.js'
import { announcementFields } from './class/announcement.js'
import { postFields } from './class/post.js'
import { classRoomFields } from './class/classRoom.js'
import { timetableItemFields, timetableFields } from './class/timetable.js'
import { stripeExpressTokenFields } from './payment/index.js'

const generalFields = {
  createdAt: 'createdAt',
  fileUploads: 'fileUploads',
  objectId: 'objectId',
  school: 'school',
  updatedAt: 'updatedAt',
}

export {
  announcementFields,
  assignmentFeedbackFields,
  assignmentFields,
  assignmentSubmissionFields,
  attendanceFields,
  chatFields,
  classRoomFields,
  courseFeedbackFields,
  classWorkFields,
  courseFields,
  fcmTokenFields,
  feedbackFields,
  fileMetadataFields,
  fileUploadFields,
  generalFields,
  averageOverrideFields,
  gradeFields,
  gradeTypeFields,
  meetingServerFields,
  messageFields,
  notificationClassFields,
  notificationFields,
  notificationRouteFields,
  paymentDataFields,
  postFields,
  quizBlankAnswerFields,
  quizBlankItemFields,
  quizChoiceOptionFields,
  quizDropdownAnswerFields,
  quizDropdownItemFields,
  quizFields,
  quizItemFields,
  quizItemTypeFields,
  quizQuestionFields,
  quizStatusFields,
  quizStatuses,
  quizTakeFields,
  quizTextAnswerFields,
  quizUploadAnswerFields,
  roleFields,
  roomFields,
  schoolFields,
  stripeExpressTokenFields,
  subjectFields,
  timetableFields,
  timetableItemFields,
  unreadChatFields,
  userFields,
  zoomRoomFields,
}
