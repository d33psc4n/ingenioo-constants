const announcementFields = {
  stripePriceId: 'stripePriceId',
  user: 'user',
}

const announcementActions = {
  createAnnouncement: 'createAnnouncement',
  deleteAnnouncement: 'deleteAnnouncement',
  getAllAnnouncements: 'getAllAnnouncements',
}

export { announcementFields, announcementActions }
