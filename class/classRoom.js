const classRoomFields = {
  group: 'group',
  students: 'students',
  timetable: 'timetable',
  classMaster: 'classMaster',
  studyYear: 'studyYear',
  identifier: 'identifier',
  schoolYear: 'schoolYear',
}

const classRoomActions = {
  addEditClassRoom: 'addEditClassRoom',
  deleteClassRoom: 'deleteClassRoom',
}

export { classRoomFields, classRoomActions }
